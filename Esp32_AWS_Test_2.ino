#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "Adafruit_TCS34725.h"

#include <Adafruit_MLX90614.h>
#include "Adafruit_TCS34725.h"

Adafruit_MLX90614 mlx = Adafruit_MLX90614();


Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_1X);

void connectAWSIoT();
void mqttCallback (char* topic, byte* payload, unsigned int length);




const int ledPin = 2;// the number of the LED pin
int ledState = LOW; // ledState used to set the LED
int interval = 100; // interval at which to blink (milliseconds)
// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0; // will store last t



char *ssid = "LVMW";
char *password = "WhiteTowers";

const char *endpoint = "a1gznvjqiygiy4-ats.iot.us-east-1.amazonaws.com";
// Example: xxxxxxxxxxxxxx.iot.ap-northeast-1.amazonaws.com
const int port = 8883;
char *pubTopic = "$aws/things/MyESPthing/shadow/update";
char *subTopic = "$aws/things/MyESPthing/shadow/update/delta";

const char* rootCA = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF
ADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6
b24gUm9vdCBDQSAxMB4XDTE1MDUyNjAwMDAwMFoXDTM4MDExNzAwMDAwMFowOTEL
MAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv
b3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj
ca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM
9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw
IFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6
VOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L
93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm
jgSubJrIqg0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC
AYYwHQYDVR0OBBYEFIQYzIU07LwMlJQuCFmcx7IQTgoIMA0GCSqGSIb3DQEBCwUA
A4IBAQCY8jdaQZChGsV2USggNiMOruYou6r4lK5IpDB/G/wkjUu0yKGX9rbxenDI
U5PMCCjjmCXPI6T53iHTfIUJrU6adTrCC2qJeHZERxhlbI1Bjjt/msv0tadQ1wUs
N+gDS63pYaACbvXy8MWy7Vu33PqUXHeeE6V/Uq2V8viTO96LXFvKWlJbYK8U90vv
o/ufQJVtMVT8QtPHRh8jrdkPSHCa2XV4cdFyQzR1bldZwgJcJmApzyMZFo6IQ6XU
5MsI+yMRQ+hDKXJioaldXgjUkK642M4UwtBV8ob2xJNDd2ZhwLnoQdeXeGADbkpy
rqXRfboQnoZsG4q5WTP468SQvvG5
-----END CERTIFICATE-----
)EOF";

const char* certificate =  R"KEY(
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVAMNR89/qaNwfyttyjR1HMsKgKKq9MA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0xOTAzMTMyMDI4
MTNaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDACOu3odAqoaA5nMqt
l/HnGA7UCs0Bdq1PxKFsmm9PxgK10/fTJDv/zU60dUbtRF4m1VQilHOW3GM6zcNo
5sB5a64MllxA5e3Jjkfa4dOr3snDSF7BhJe+wH5pXgUKgK5GhoBgjJZrkVFiSrPj
NgO8twVRdACnL9PBvvXWs+tp1UMWiRPvlGgzfNSVSnOyU+Pf85FffG7V0SElcFtX
nZHZhji4yimh2sjU7UP0t69JXKejMriuiqkRmZzbJwjNoDJGzVQg1jc8huryGk4b
c/qD4aHoU5mspz36yLrIkeHeeHNciwEIqZS76PaQXbghfNM4/ySzArJBEd3tF3l8
xN9BAgMBAAGjYDBeMB8GA1UdIwQYMBaAFOz1QbAEeERxKpiyXLhGIsHbC8wdMB0G
A1UdDgQWBBRDZIJLRXSa3O3t+XqlqUs++lVhiTAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAj+8eb20Ww5T8Zr+FdNVt7PWt
GhJZ5MT2k/oO/D+/2pjyJyNcb2eltQmCClLexGmCPJ0jXkUCEHV8/0wHM8BrFSva
3QuHoSGOZcV4z5PXab27DUgAOihoRFxrx7Q0/rOM+KIJT9iFiXjPvwBKWaxMXvhA
wd8tCdJCxwtThtiugxov6R3Z+OHB/tIUkxQvOXalOJeWWhlIz/N7mbnG/AJ5kxXq
Z6p/Z80hUUUxIM9IywWvcMdSwpvGYRiTeN8WuYHEDVi7f6856xvIGxthtPmxaZiS
M6UjZKD0/ER8TWLC6zTrvbgPKWeRI+WFiT+dBOIO1mEMpMHOH1abjV/+Xp54OA==
-----END CERTIFICATE-----
)KEY";

const char* privateKey =  R"KEY(
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAwAjrt6HQKqGgOZzKrZfx5xgO1ArNAXatT8ShbJpvT8YCtdP3
0yQ7/81OtHVG7UReJtVUIpRzltxjOs3DaObAeWuuDJZcQOXtyY5H2uHTq97Jw0he
wYSXvsB+aV4FCoCuRoaAYIyWa5FRYkqz4zYDvLcFUXQApy/Twb711rPradVDFokT
75RoM3zUlUpzslPj3/ORX3xu1dEhJXBbV52R2YY4uMopodrI1O1D9LevSVynozK4
roqpEZmc2ycIzaAyRs1UINY3PIbq8hpOG3P6g+Gh6FOZrKc9+si6yJHh3nhzXIsB
CKmUu+j2kF24IXzTOP8kswKyQRHd7Rd5fMTfQQIDAQABAoIBAHkc5zVQe453kyri
xT0IqIs8NBXtne//Xj0B4WEyYWOpmYn93r4uixzA6pOLp6j6xnqtbrvEbsZFxtuO
bGDbr6akkAfmSNaxbPACqTTV2n76KrvcB877Bg1LMC7aGknOcJuQNqUt5ax/4Rkh
ziRNq7u4nkUJ6WOIGYCHJFs97Ao5W6IEfVt+cMcYl48jeaqG3JRnDiqTt4SWohnR
QSYfhgu2INjx/KLDPAmln17KM2LHx1LqFT661iuNG7pvd5kc9nYW7B2Pl/5vB07G
Pl6vaFiE6iUC6FOSOUf4uWWgAysxzI+ASyl8VGgEAUX10M8vJhlqpg5eXhwI7mZO
vfLtuMECgYEA5HZ8MsThendXk7iKJ3eDY4Aw8jHlsBgFjrJ8BG8KYxerLiARJLfS
ZL4GNUoPBqkaAkr6FLd04Z+uiLiJbpFdtOyUzo17xBtZweBLv7jgZCzoqKoYVbep
OljLllTQtEi0Pfj9tQObyo7QK0IsYK/2+2Zhkyiruxf0mRRIiFtOfakCgYEA1y5n
OSc0/hbDAzzYOY/uCCkvPEMsnGohknVpPm3WotYSCX5gTgANRD4iil5rFLK/SQMp
e9gd2AEYDgmPspjv8BUStWZKXiIUneaPSzwI/jAxXLmh51dWRIcwaSSo6a7uZQjM
TOHL/u5b/i+iCq4T8Oc94wq2OFVWHHA6hFYEY9kCgYA6WVCLHuF/h69kWQNf5RQ3
e2wEz12cReZVdZxa/Yknmcnsc+AtAo3HkQjA33NK/jFYWJHabKh7X28SGB9xj8rh
BHNU9amggmPAMmrmQm985kNdoGOqsMeV0Uvdzzemc9Mk6MzTSGKnryXXhoUTBJ4N
wy40iXSiyhF8b0O/H6LCUQKBgQCPed9V2nCZIzAwjndWGkQKqPtX4SpzuMhd0W+1
MqO0u6wXLG6mU56E7DE3tbEMggrNchBNBVMQ3dneMj3gRd1mZYat8WCQ14SLrFnL
PmJ4qkWfoB72E+PvmXyL/uepuPw33d/rO3s7pk8sczJGkL+3F1jKdC//69aY67P4
3SncMQKBgQCJNTLySTyZservOGAtqiRUoEzGEA3k4sq8oFadQHYjFlwJyTuTCaCG
0d4jCw4fdOYn37NVDxpJAsrZSE2IeQnlUIi1kJo+HBIthbafRJba1ke/mafbxiE9
uAjwrPcREwdR5YuSme7Yx6i2o10tvuUavBlIWcvmzFEz6iIhN00jcA==
-----END RSA PRIVATE KEY-----
)KEY";

WiFiClientSecure httpsClient;
PubSubClient mqttClient(httpsClient);



void WIFI_Connect()
{
WiFi.disconnect();
Serial.println("Connecting to WiFi...");

 WiFi.begin(ssid, password);


for (int i = 0; i < 15; i++)
{
if ( WiFi.status() != WL_CONNECTED )
{
delay ( 250 );
digitalWrite(ledPin, LOW);
Serial.print ( "." );
delay ( 250 );
digitalWrite(ledPin, HIGH);
}
}
if ( WiFi.status() == WL_CONNECTED )
{
Serial.println("");
Serial.println("WiFi Connected");
Serial.println("IP address: ");
Serial.println(WiFi.localIP());
Serial.println("");
Serial.println("Setting Device Certificates");
Serial.println("");
  httpsClient.setCACert(rootCA);
    httpsClient.setCertificate(certificate);
    httpsClient.setPrivateKey(privateKey);
    mqttClient.setServer(endpoint, port);
    mqttClient.setCallback(mqttCallback);
    mqttClient.setClient(httpsClient);

//Serial.println("Trying to connect to MQTT OLT...");

  connectAWSIoT(); // uncomment here to mqtt connection
  

}
digitalWrite(ledPin, 0);
}





void setup() {
    delay(1000);
        pinMode (ledPin, OUTPUT);
    Serial.begin(115200);
     Serial.println("Adafruit MLX90614 test");  
     mlx.begin();  

    // Start WiFi
    Serial.println("Connecting to ");
    Serial.print(ssid);
    WiFi.begin(ssid, password);

    WiFi.disconnect(true);

}

void connectAWSIoT() {
    while (!mqttClient.connected()) {
        if (mqttClient.connect("ESP32_device")) {
            Serial.println("MQTT Connected.");
            int qos = 0;
            mqttClient.subscribe(subTopic, qos);
            Serial.println("MQTT Subscribed.");
        } else {
            Serial.print("Failed. Error state=");
            Serial.print(mqttClient.state());
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

long messageSentAt = 0;
int dummyValue = 0;
char pubMessage[128];
char pubMessage2[128];
char pubMessage3[128];

void mqttCallback (char* topic, byte* payload, unsigned int length) {
    Serial.print("Received. topic=");
    Serial.println(topic);
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
    }
    Serial.print("\n");
}

void mqttLoop() {
    if (!mqttClient.connected()) {
        connectAWSIoT();
    }
    mqttClient.loop();


    long now = millis();
    
    uint16_t r, g, b, c, colorTemp, lux;
  
    tcs.getRawData(&r, &g, &b, &c);
    colorTemp = tcs.calculateColorTemperature(r, g, b);
    lux = tcs.calculateLux(r, g, b);
  

    if (now - messageSentAt > 5000) {

  Serial.print("Color Temp: "); Serial.print(colorTemp, DEC); Serial.print(" K - ");
  Serial.print("Lux: "); Serial.print(lux, DEC); Serial.print(" - ");
  Serial.print("R: "); Serial.print(r, DEC); Serial.print(" ");
  Serial.print("G: "); Serial.print(g, DEC); Serial.print(" ");
  Serial.print("B: "); Serial.print(b, DEC); Serial.print(" ");
  Serial.print("C: "); Serial.print(c, DEC); Serial.print(" ");
  Serial.println(" ");



   float MLX_AmbTemp; //degC
   float MLX_ObjTemp; //%

   MLX_AmbTemp = mlx.readAmbientTempC();
   MLX_ObjTemp = mlx.readObjectTempC();
  
  Serial.print("Ambient = "); Serial.print( MLX_AmbTemp); 
  Serial.print("*C\tObject = "); Serial.print(   MLX_ObjTemp ); Serial.println("*C");
  //Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempF()); 
  //Serial.print("*F\tObject = "); Serial.print(mlx.readObjectTempF()); Serial.println("*F");


      
        messageSentAt = now;
       // sprintf(pubMessage, "{\"state\": {\"desired\":{\"foo\":\"%d\"}}}", dummyValue++);
        sprintf(pubMessage, "{\"RGB\": {\"values\":{\"RED: \":\"%d\",\"GREEN: \":\"%d\",\"BLUE: \":\"%d\"}}}", r,g,b);
       
        Serial.print("Publishing message to topic ");
        Serial.println(pubTopic);
        Serial.println(pubMessage);
        mqttClient.publish(pubTopic, pubMessage);
        Serial.println("Published.");

       // sprintf(pubMessage, "{\"state\": {\"desired\":{\"foo\":\"%d\"}}}", dummyValue++);
        sprintf(pubMessage2, "{\"LUX\": {\"values\":{\"LUX: \":\"%d\"}}}",lux);
       
        Serial.print("Publishing message to topic ");
        Serial.println(pubTopic);
        Serial.println(pubMessage2);
        mqttClient.publish(pubTopic, pubMessage2);
        Serial.println("Published.");


        sprintf(pubMessage3, "{\"IR_Temp\": {\"values\":{\"OBJ_TEMP: \":\"%0.2f\",\"AMB_TEMP: \":\"%0.2f\" }}}",MLX_ObjTemp ,MLX_AmbTemp);
       
        Serial.print("Publishing message to topic ");
        Serial.println(pubTopic);
        Serial.println(pubMessage3);
        mqttClient.publish(pubTopic, pubMessage3);
        Serial.println("Published.");




        
    }
}

void loop() {
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval)
    {
        if (WiFi.status() != WL_CONNECTED)
            {
              Serial.println("wifi disconnected ");
              WIFI_Connect();
            }
            // save the last time you blinked the LED
      previousMillis = currentMillis;
      // if the LED is off turn it on and vice-versa:
          if (ledState == LOW)
                {
                  ledState = HIGH;
                   interval = 100;
                 }
          else
                {
                  ledState = LOW;
                  interval = 2500;
                }
       // set the LED with the ledState of the variable:
      digitalWrite(ledPin, ledState);
        
    }
    else {
          mqttLoop();
         }
}