# ESP32_AWS_CON

This repo contains the source code (.ino) to create a MQTT connection to AWS IoT using TLS certificates for the ESP32 dev board (Espressif).
The ESP32 supports WiFi and Bluetooth. ESP32 is also arduino compatible. 

A similar version of the code can be used to connect to Lightelligence.

Board compatible: NodeMCU-32S
Fash Freq: 80 MHZ

Please note that  in this example, the certificates are defined inside the ino file. They could be written to a header file (.h) to allow for code portability/reusability by changing only the imported .h file .

The example publishes data of two I2C sensors:

1. MLX90614 single pixel infrared temperature sensor.
2. Adafruit TCS4725 color sensor.

Please refer to Adafruit tutorials for sensor pins and wiring and libraries installation.

https://learn.adafruit.com/using-melexis-mlx90614-non-contact-sensors/wiring-and-test

https://learn.adafruit.com/adafruit-color-sensors/overview





Here is a tutorial showing how to install the ESP32 board to the Arduino IDE. 

https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/

Or in the Arduino IDE, do the following:
Tools -> Boards Manager -> Then type: "esp32" and select the option by Espressif Systems and install.


![A test image](Esp32boardinstallation.PNG)